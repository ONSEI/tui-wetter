#!/usr/bin/env python3

from snipsTools import SnipsConfigParser
from hermes_python.hermes import Hermes
import requests

CONFIG_INI = "config.ini"

MQTT_IP_ADDR = "localhost"
MQTT_PORT = 1883
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT))
USERNAME_INTENTS = "jsemig"
SHIP_DATA_URL = 'https://tuic-ms3-api.prelive.cellular.de/cruise/info'


class TuiTimeAndDate(object):
    """Class used to wrap action code with mqtt connection

        Please change the name refering to your application
    """

    def __init__(self):
        # get the configuration if needed
        try:
            self.config = SnipsConfigParser.read_configuration_file(CONFIG_INI)
        except:
            self.config = None

        # start listening to MQTT
        self.start_blocking()

    @staticmethod
    def user_intent(intentname):
        return USERNAME_INTENTS + ":" + intentname

    @staticmethod
    def get_ship_data():
        r = requests.get(SHIP_DATA_URL)
        if r.status_code < 300:
            return r.json()

    def grad_intent(self, hermes, intent_message):
        ship_data = self.get_ship_data()
        result_sentence = "Die Temperatur in diesem Raum ist {0} Grad. " \
                          "Die Außentemperatur beträgt {1} Grad" \
                          " und die Wassertemperatur {2} Grad.  " \
            .format('0',
                    ship_data['airtemp_c'],
                    ship_data['watertemp_c'])
        current_session_id = intent_message.session_id
        hermes.publish_end_session(current_session_id, result_sentence)


    def wetterbericht_intent(self, hermes, intent_message):
        ship_data = self.get_ship_data()
        result_sentence = "Es ist {0} und {1} mit einer Windgeschwindigkeit von " \
                          "{2} Kilometer pro Stunde, Wassertemperatur von {3} Grad " \
                          "und einer aussentemperatur von {4} Grad"\
            .format(ship_data['conditions1_german'],
                    ship_data['conditions2_german'],
                    ship_data['windspeed_true_kmh'],
                    ship_data['watertemp_c'],
                    ship_data['airtemp_c'])
        current_session_id = intent_message.session_id
        hermes.publish_end_session(current_session_id, result_sentence)

    # --> Master callback function, triggered everytime an intent is recognized
    def master_intent_callback(self, hermes, intent_message):
        coming_intent = intent_message.intent.intent_name
        if coming_intent == self.user_intent('Grad'):
            self.grad_intent(hermes, intent_message)
        elif coming_intent == self.user_intent('Wetterbericht'):
            self.wetterbericht_intent(hermes, intent_message)

    # --> Register callback function and start MQTT
    def start_blocking(self):
        with Hermes(MQTT_ADDR) as h:
            h.subscribe_intents(self.master_intent_callback).start()


if __name__ == "__main__":
    TuiTimeAndDate()
